
HYPERPARAM_SEMANTICTYPE_MAPPING = {
    "n_jobs": "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter"
}

TUNING_HYPERPARAM = "https://metadata.datadrivendiscovery.org/types/TuningParameter"
