from typing import Any, Callable, List, Dict, Union, Optional, Sequence, Tuple
from numpy import ndarray
from collections import OrderedDict
from scipy import sparse
import os
import sklearn

# Custom import commands if any
{% for key, value in modules.items() %}from {{ key }} import {{ value }}{% endfor %}
{% for import in custom_imports %}{{ import }}
{% endfor %}

from d3m.container.numpy import ndarray as d3m_ndarray
from d3m.container import DataFrame as d3m_dataframe
from d3m.metadata import hyperparams, params, base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.base import CallResult, DockerContainer
{% block interface %}from d3m.primitive_interfaces.base import PrimitiveBase{% endblock %}


Inputs = d3m_dataframe
Outputs = d3m_dataframe

{% if params|length == 0 %}
Params = None
{% else %}
class Params(params.Params):{% for key, value in params.items() %}
    {{ key }}: Optional[{{ value}}]{% endfor %}
{% endif %}

class Hyperparams(hyperparams.Hyperparams):{% for h in hyperparams %}{% if h['name'] != 'random_state' %}
    {{ h['name'] }} = hyperparams.{{ h['type'] }}{% if h['type'] in ['Enumeration', 'Hyperparameter', 'Bounded'] %}[{{ h['init_args']['_structural_type'] }}]{% endif %}(
    {% if h['type'] == 'Union' %}    OrderedDict({ {% for h_param in h['hyperparams'] %}
           "{{ h_param['name'] }}": hyperparams.{{ h_param['type'] }}{% if h_param['type'] in ['Enumeration', 'Hyperparameter', 'Bounded'] %}[{{ h_param['init_args']['_structural_type'] }}]{% endif %}(
           {% for arg, value in h_param['init_args'].items() %}{% if not arg == '_structural_type' %}          {{ arg }}={{ value }},{% endif %}
           {% endfor %}),{% endfor %}
        }),
    {% endif %}{% for arg, value in h['init_args'].items() %}{% if not arg == '_structural_type' %}    {{ arg }}={{ value }}{% if not loop.last %},{% endif %}
    {% endif %}{% endfor %}){% endif %}{% endfor %}{% if hyperparams|length ==0 %}pass{% endif %}


{% block classname %}{% endblock %}
    """
    Primitive wrapping for sklearn {{ class_name }}
    """

    __author__ = "JPL MARVIN"
    metadata = metadata_module.PrimitiveMetadata({ {% for key, value in metadata.items() %}
         "{{ key }}": {{ value }},{% endfor %}
         'installation': [{'type': metadata_module.PrimitiveInstallationType.PIP,
                           'package_uri': 'git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@{git_commit}#egg=sklearn_wrap'.format(
                               git_commit=utils.current_git_commit(os.path.dirname(__file__)),
                            ),
                           }]
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None{% for arg in constructor_args %}{% if not arg['name'] == 'random_state' %},
                 _{{ arg['name'] }}: {{ arg['type'] }} = {{ arg['default'] }}{% endif %}{% endfor %}) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._clf = {{ class_name }}({% for h in hyperparams %}
            {{ h['name'] }}={% if h['eval'] %}eval(self.hyperparams['{{ h['name'] }}']){% else %}self.hyperparams['{{ h['name'] }}']{% endif %},{% endfor %}{% for arg in constructor_args %}
            {% if arg['name'] == 'random_state' %}random_state=self.random_seed,{% else %}{{ arg['name'] }}=_{{ arg['name'] }}{% if not loop.last %},{% endif %}{% endif %}{% endfor %}
        ){% block class_variables %}{% endblock %}
        {% block set_training_data %}{% endblock %}{% block fit %}{% endblock %}{% block produce %}{% endblock %}{% block get_set_params %}{% endblock %}
SK{{ class_name }}.__doc__ = {{ class_name }}.__doc__
