{
  "is_class": true, 
  "library": "sklearn", 
  "compute_resources": {}, 
  "common_name": "Random Trees Embedding", 
  "id": "87315345-128a-30fb-bd14-d6f690383546", 
  "category": "ensemble.forest", 
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/ensemble/forest.py#L1515", 
  "parameters": [
    {
      "default": "10", 
      "type": "integer", 
      "optional": "true", 
      "name": "n_estimators", 
      "description": "Number of trees in the forest. "
    }, 
    {
      "default": "5", 
      "type": "integer", 
      "optional": "true", 
      "name": "max_depth", 
      "description": "The maximum depth of each tree. If None, then nodes are expanded until all leaves are pure or until all leaves contain less than min_samples_split samples. "
    }, 
    {
      "default": "2", 
      "type": "int", 
      "optional": "true", 
      "name": "min_samples_split", 
      "description": "The minimum number of samples required to split an internal node:  - If int, then consider `min_samples_split` as the minimum number. - If float, then `min_samples_split` is a percentage and `ceil(min_samples_split * n_samples)` is the minimum number of samples for each split.  .. versionchanged:: 0.18 Added float values for percentages. "
    }, 
    {
      "default": "1", 
      "type": "int", 
      "optional": "true", 
      "name": "min_samples_leaf", 
      "description": "The minimum number of samples required to be at a leaf node:  - If int, then consider `min_samples_leaf` as the minimum number. - If float, then `min_samples_leaf` is a percentage and `ceil(min_samples_leaf * n_samples)` is the minimum number of samples for each node.  .. versionchanged:: 0.18 Added float values for percentages. "
    }, 
    {
      "default": "0.", 
      "type": "float", 
      "optional": "true", 
      "name": "min_weight_fraction_leaf", 
      "description": "The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided. "
    }, 
    {
      "default": "None", 
      "type": "int", 
      "optional": "true", 
      "name": "max_leaf_nodes", 
      "description": "Grow trees with ``max_leaf_nodes`` in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes. "
    }, 
    {
      "default": "1e-7", 
      "type": "float", 
      "optional": "true", 
      "name": "min_impurity_split", 
      "description": "Threshold for early stopping in tree growth. A node will split if its impurity is above the threshold, otherwise it is a leaf.  .. versionadded:: 0.18 "
    }, 
    {
      "default": "True", 
      "type": "bool", 
      "optional": "true", 
      "name": "sparse_output", 
      "description": "Whether or not to return a sparse CSR matrix, as default behavior, or to return a dense array compatible with dense pipeline operators. "
    }, 
    {
      "default": "1", 
      "type": "integer", 
      "optional": "true", 
      "name": "n_jobs", 
      "description": "The number of jobs to run in parallel for both `fit` and `predict`. If -1, then the number of jobs is set to the number of cores. "
    }, 
    {
      "default": "None", 
      "type": "int", 
      "optional": "true", 
      "name": "random_state", 
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    }, 
    {
      "default": "0", 
      "type": "int", 
      "optional": "true", 
      "name": "verbose", 
      "description": "Controls the verbosity of the tree building process. "
    }, 
    {
      "default": "False", 
      "type": "bool", 
      "optional": "true", 
      "name": "warm_start", 
      "description": "When set to ``True``, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just fit a whole new forest. "
    }
  ], 
  "tags": [
    "ensemble", 
    "forest"
  ], 
  "common_name_unanalyzed": "Random Trees Embedding", 
  "schema_version": 1.0, 
  "languages": [
    "python2.7"
  ], 
  "version": "0.18.1", 
  "build": [
    {
      "type": "pip", 
      "package": "scikit-learn"
    }
  ], 
  "description": "'An ensemble of totally random trees.\n\nAn unsupervised transformation of a dataset to a high-dimensional\nsparse representation. A datapoint is coded according to which leaf of\neach tree it is sorted into. Using a one-hot encoding of the leaves,\nthis leads to a binary coding with as many ones as there are trees in\nthe forest.\n\nThe dimensionality of the resulting representation is\n``n_out <= n_estimators * max_leaf_nodes``. If ``max_leaf_nodes == None``,\nthe number of leaf nodes is at most ``n_estimators * 2 ** max_depth``.\n\nRead more in the :ref:`User Guide <random_trees_embedding>`.\n", 
  "methods_available": [
    {
      "id": "sklearn.ensemble.forest.RandomTreesEmbedding.apply", 
      "returns": {
        "shape": "n_samples, n_estimators", 
        "type": "array", 
        "name": "X_leaves", 
        "description": "For each datapoint x in X and for each tree in the forest, return the index of the leaf x ends up in. '"
      }, 
      "description": "'Apply trees in the forest to X, return leaf indices.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The input samples. Internally, its dtype will be converted to ``dtype=np.float32``. If a sparse matrix is provided, it will be converted into a sparse ``csr_matrix``. "
        }
      ], 
      "name": "apply"
    }, 
    {
      "id": "sklearn.ensemble.forest.RandomTreesEmbedding.decision_path", 
      "returns": {
        "shape": "n_samples, n_nodes", 
        "type": "sparse", 
        "name": "indicator", 
        "description": "Return a node indicator matrix where non zero elements indicates that the samples goes through the nodes.  n_nodes_ptr : array of size (n_estimators + 1, ) The columns from indicator[n_nodes_ptr[i]:n_nodes_ptr[i+1]] gives the indicator value for the i-th estimator.  '"
      }, 
      "description": "'Return the decision path in the forest\n\n.. versionadded:: 0.18\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The input samples. Internally, its dtype will be converted to ``dtype=np.float32``. If a sparse matrix is provided, it will be converted into a sparse ``csr_matrix``. "
        }
      ], 
      "name": "decision_path"
    }, 
    {
      "id": "sklearn.ensemble.forest.RandomTreesEmbedding.fit", 
      "returns": {
        "type": "object", 
        "name": "self", 
        "description": "Returns self.  '"
      }, 
      "description": "'Fit estimator.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The input samples. Use ``dtype=np.float32`` for maximum efficiency. Sparse matrices are also supported, use sparse ``csc_matrix`` for maximum efficiency. "
        }
      ], 
      "name": "fit"
    }, 
    {
      "id": "sklearn.ensemble.forest.RandomTreesEmbedding.fit_transform", 
      "returns": {
        "shape": "n_samples, n_out", 
        "type": "sparse", 
        "name": "X_transformed", 
        "description": "Transformed dataset. '"
      }, 
      "description": "'Fit estimator and transform dataset.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Input data used to build forests. Use ``dtype=np.float32`` for maximum efficiency. "
        }
      ], 
      "name": "fit_transform"
    }, 
    {
      "id": "sklearn.ensemble.forest.RandomTreesEmbedding.get_params", 
      "returns": {
        "type": "mapping", 
        "name": "params", 
        "description": "Parameter names mapped to their values. '"
      }, 
      "description": "'Get parameters for this estimator.\n", 
      "parameters": [
        {
          "type": "boolean", 
          "optional": "true", 
          "name": "deep", 
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ], 
      "name": "get_params"
    }, 
    {
      "id": "sklearn.ensemble.forest.RandomTreesEmbedding.set_params", 
      "returns": {
        "name": "self", 
        "description": "\""
      }, 
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n", 
      "parameters": [], 
      "name": "set_params"
    }, 
    {
      "id": "sklearn.ensemble.forest.RandomTreesEmbedding.transform", 
      "returns": {
        "shape": "n_samples, n_out", 
        "type": "sparse", 
        "name": "X_transformed", 
        "description": "Transformed dataset. '"
      }, 
      "description": "'Transform dataset.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Input data to be transformed. Use ``dtype=np.float32`` for maximum efficiency. Sparse matrices are also supported, use sparse ``csr_matrix`` for maximum efficiency. "
        }
      ], 
      "name": "transform"
    }
  ], 
  "learning_type": [
    "supervised", 
    "unsupervised"
  ], 
  "task_type": [
    "modeling", 
    "data preprocessing"
  ], 
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/ensemble/forest.py#L1515", 
  "category_unanalyzed": "ensemble.forest", 
  "name": "sklearn.ensemble.forest.RandomTreesEmbedding", 
  "team": "jpl", 
  "attributes": [
    {
      "type": "list", 
      "name": "estimators_", 
      "description": "The collection of fitted sub-estimators. "
    }
  ]
}