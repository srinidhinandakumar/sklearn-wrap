{
  "handles_regression": false,
  "is_class": true,
  "library": "sklearn",
  "compute_resources": {},
  "common_name": "Passive Aggressive Classifier",
  "id": "2cb29a54-f611-3878-ab0f-db2a39239a3e",
  "handles_classification": true,
  "category": "linear_model.passive_aggressive",
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/linear_model/passive_aggressive.py#L9",
  "parameters": [
    {
      "type": "bool",
      "name": "fit_intercept",
      "description": "Whether the intercept should be estimated or not. If False, the data is assumed to be already centered. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "n_iter",
      "description": "The number of passes over the training data (aka epochs). Defaults to 5. "
    },
    {
      "type": "bool",
      "name": "shuffle",
      "description": "Whether or not the training data should be shuffled after each epoch. "
    },
    {
      "type": "int",
      "name": "random_state",
      "description": "The seed of the pseudo random number generator to use when shuffling the data. "
    },
    {
      "type": "integer",
      "optional": "true",
      "name": "verbose",
      "description": "The verbosity level "
    },
    {
      "type": "integer",
      "optional": "true",
      "name": "n_jobs",
      "description": "The number of CPUs to use to do the OVA (One Versus All, for multi-class problems) computation. -1 means \\'all CPUs\\'. Defaults to 1. "
    },
    {
      "type": "string",
      "optional": "true",
      "name": "loss",
      "description": "The loss function to be used: hinge: equivalent to PA-I in the reference paper. squared_hinge: equivalent to PA-II in the reference paper. "
    },
    {
      "type": "bool",
      "optional": "true",
      "name": "warm_start",
      "description": "When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution. "
    },
    {
      "type": "dict",
      "name": "class_weight",
      "description": "Preset for the class_weight fit parameter.  Weights associated with classes. If not given, all classes are supposed to have weight one.  The \"balanced\" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))``  .. versionadded:: 0.17 parameter *class_weight* to automatically weight samples. "
    }
  ],
  "input_type": [
    "DENSE",
    "SPARSE",
    "UNSIGNED_DATA"
  ],
  "methods_available": [
    {
      "id": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier.decision_function",
      "returns": {
        "name": "array, shape=(n_samples,) if n_classes == 2 else (n_samples, n_classes)",
        "description": "Confidence scores per (sample, class) combination. In the binary case, confidence score for self.classes_[1] where >0 means this class would be predicted. '"
      },
      "description": "'Predict confidence scores for samples.\n\nThe confidence score for a sample is the signed distance of that\nsample to the hyperplane.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "name": "decision_function"
    },
    {
      "id": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier.densify",
      "returns": {
        "name": "self: estimator",
        "description": "'"
      },
      "description": "'Convert coefficient matrix to dense array format.\n\nConverts the ``coef_`` member (back) to a numpy.ndarray. This is the\ndefault format of ``coef_`` and is required for fitting, so calling\nthis method is only required on models that have previously been\nsparsified; otherwise, it is a no-op.\n",
      "parameters": [],
      "name": "densify"
    },
    {
      "id": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier.fit",
      "returns": {
        "type": "returns",
        "name": "self",
        "description": "'"
      },
      "description": "'Fit linear model with Passive Aggressive algorithm.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Training data "
        },
        {
          "shape": "n_samples",
          "type": "numpy",
          "name": "y",
          "description": "Target values "
        },
        {
          "shape": "n_classes,n_features",
          "type": "array",
          "name": "coef_init",
          "description": "The initial coefficients to warm-start the optimization. "
        },
        {
          "shape": "n_classes",
          "type": "array",
          "name": "intercept_init",
          "description": "The initial intercept to warm-start the optimization. "
        }
      ],
      "name": "fit"
    },
    {
      "id": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier.get_params",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      },
      "description": "'Get parameters for this estimator.\n",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "name": "get_params"
    },
    {
      "id": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier.partial_fit",
      "returns": {
        "type": "returns",
        "name": "self",
        "description": "\""
      },
      "description": "\"Fit linear model with Passive Aggressive algorithm.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Subset of the training data "
        },
        {
          "shape": "n_samples",
          "type": "numpy",
          "name": "y",
          "description": "Subset of the target values "
        },
        {
          "shape": "n_classes",
          "type": "array",
          "name": "classes",
          "description": "Classes across all calls to partial_fit. Can be obtained by via `np.unique(y_all)`, where y_all is the target vector of the entire dataset. This argument is required for the first call to partial_fit and can be omitted in the subsequent calls. Note that y doesn't need to contain all labels in `classes`. "
        }
      ],
      "name": "partial_fit"
    },
    {
      "id": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier.predict",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "C",
        "description": "Predicted class label per sample. '"
      },
      "description": "'Predict class labels for samples in X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "name": "predict"
    },
    {
      "id": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier.score",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      },
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "name": "score"
    },
    {
      "id": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier.set_params",
      "description": "None",
      "parameters": [],
      "name": "set_params"
    },
    {
      "id": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier.sparsify",
      "returns": {
        "name": "self: estimator",
        "description": "'"
      },
      "description": "'Convert coefficient matrix to sparse format.\n\nConverts the ``coef_`` member to a scipy.sparse matrix, which for\nL1-regularized models can be much more memory- and storage-efficient\nthan the usual numpy.ndarray representation.\n\nThe ``intercept_`` member is not converted.\n\nNotes\n-----\nFor non-sparse models, i.e. when there are not many zeros in ``coef_``,\nthis may actually *increase* memory usage, so use this method with\ncare. A rule of thumb is that the number of zero elements, which can\nbe computed with ``(coef_ == 0).sum()``, must be more than 50% for this\nto provide significant benefits.\n\nAfter calling this method, further fitting with the partial_fit\nmethod (if any) will not work until you call densify.\n",
      "parameters": [],
      "name": "sparsify"
    }
  ],
  "common_name_unanalyzed": "Passive Aggressive Classifier",
  "schema_version": 1.0,
  "languages": [
    "python2.7"
  ],
  "version": "0.18.1",
  "build": [
    {
      "type": "pip",
      "package": "scikit-learn"
    }
  ],
  "handles_multiclass": true,
  "description": "'Passive Aggressive Classifier\n\nRead more in the :ref:`User Guide <passive_aggressive>`.\n",
  "tags": [
    "passive_aggressive",
    "linear_model",
    "classification"
  ],
  "algorithm_type": [
    "classification"
  ],
  "learning_type": [
    "supervised"
  ],
  "task_type": [
    "modeling"
  ],
  "output_type": [
    "PREDICTIONS"
  ],
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/linear_model/passive_aggressive.py#L9",
  "category_unanalyzed": "linear_model.passive_aggressive",
  "name": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier",
  "handles_multilabel": true,
  "is_deterministic": true,
  "team": "jpl",
  "attributes": [
    {
      "shape": "1, n_features",
      "type": "array",
      "name": "coef_",
      "description": "Weights assigned to the features. "
    },
    {
      "shape": "1",
      "type": "array",
      "name": "intercept_",
      "description": "Constants in decision function.  See also --------  SGDClassifier Perceptron "
    }
  ]
}
