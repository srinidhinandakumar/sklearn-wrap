{
  "handles_regression": true, 
  "is_class": true, 
  "library": "sklearn", 
  "compute_resources": {}, 
  "common_name": "Gradient Boosting Regressor", 
  "id": "615ffc7f-ea90-3e80-8717-004d1bf4040d", 
  "handles_classification": false, 
  "category": "ensemble.gradient_boosting", 
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/ensemble/gradient_boosting.py#L1635", 
  "parameters": [
    {
      "default": "\\'ls\\'", 
      "type": "\\'ls\\', \\'lad\\', \\'huber\\', \\'quantile\\'", 
      "optional": "true", 
      "name": "loss", 
      "description": "loss function to be optimized. \\'ls\\' refers to least squares regression. \\'lad\\' (least absolute deviation) is a highly robust loss function solely based on order information of the input variables. \\'huber\\' is a combination of the two. \\'quantile\\' allows quantile regression (use `alpha` to specify the quantile). "
    }, 
    {
      "default": "0.1", 
      "type": "float", 
      "optional": "true", 
      "name": "learning_rate", 
      "description": "learning rate shrinks the contribution of each tree by `learning_rate`. There is a trade-off between learning_rate and n_estimators. "
    }, 
    {
      "type": "int", 
      "name": "n_estimators", 
      "description": "The number of boosting stages to perform. Gradient boosting is fairly robust to over-fitting so a large number usually results in better performance. "
    }, 
    {
      "default": "3", 
      "type": "integer", 
      "optional": "true", 
      "name": "max_depth", 
      "description": "maximum depth of the individual regression estimators. The maximum depth limits the number of nodes in the tree. Tune this parameter for best performance; the best value depends on the interaction of the input variables. "
    }, 
    {
      "default": "\"friedman_mse\"", 
      "type": "string", 
      "optional": "true", 
      "name": "criterion", 
      "description": "The function to measure the quality of a split. Supported criteria are \"friedman_mse\" for the mean squared error with improvement score by Friedman, \"mse\" for mean squared error, and \"mae\" for the mean absolute error. The default value of \"friedman_mse\" is generally the best as it can provide a better approximation in some cases.  .. versionadded:: 0.18 "
    }, 
    {
      "default": "2", 
      "type": "int", 
      "optional": "true", 
      "name": "min_samples_split", 
      "description": "The minimum number of samples required to split an internal node:  - If int, then consider `min_samples_split` as the minimum number. - If float, then `min_samples_split` is a percentage and `ceil(min_samples_split * n_samples)` are the minimum number of samples for each split.  .. versionchanged:: 0.18 Added float values for percentages. "
    }, 
    {
      "default": "1", 
      "type": "int", 
      "optional": "true", 
      "name": "min_samples_leaf", 
      "description": "The minimum number of samples required to be at a leaf node:  - If int, then consider `min_samples_leaf` as the minimum number. - If float, then `min_samples_leaf` is a percentage and `ceil(min_samples_leaf * n_samples)` are the minimum number of samples for each node.  .. versionchanged:: 0.18 Added float values for percentages. "
    }, 
    {
      "default": "0.", 
      "type": "float", 
      "optional": "true", 
      "name": "min_weight_fraction_leaf", 
      "description": "The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided. "
    }, 
    {
      "default": "1.0", 
      "type": "float", 
      "optional": "true", 
      "name": "subsample", 
      "description": "The fraction of samples to be used for fitting the individual base learners. If smaller than 1.0 this results in Stochastic Gradient Boosting. `subsample` interacts with the parameter `n_estimators`. Choosing `subsample < 1.0` leads to a reduction of variance and an increase in bias. "
    }, 
    {
      "default": "None", 
      "type": "int", 
      "optional": "true", 
      "name": "max_features", 
      "description": "The number of features to consider when looking for the best split:  - If int, then consider `max_features` features at each split. - If float, then `max_features` is a percentage and `int(max_features * n_features)` features are considered at each split. - If \"auto\", then `max_features=n_features`. - If \"sqrt\", then `max_features=sqrt(n_features)`. - If \"log2\", then `max_features=log2(n_features)`. - If None, then `max_features=n_features`.  Choosing `max_features < n_features` leads to a reduction of variance and an increase in bias.  Note: the search for a split does not stop until at least one valid partition of the node samples is found, even if it requires to effectively inspect more than ``max_features`` features. "
    }, 
    {
      "default": "None", 
      "type": "int", 
      "optional": "true", 
      "name": "max_leaf_nodes", 
      "description": "Grow trees with ``max_leaf_nodes`` in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes. "
    }, 
    {
      "default": "1e-7", 
      "type": "float", 
      "optional": "true", 
      "name": "min_impurity_split", 
      "description": "Threshold for early stopping in tree growth. A node will split if its impurity is above the threshold, otherwise it is a leaf.  .. versionadded:: 0.18 "
    }, 
    {
      "type": "float", 
      "name": "alpha", 
      "description": "The alpha-quantile of the huber loss function and the quantile loss function. Only if ``loss=\\'huber\\'`` or ``loss=\\'quantile\\'``. "
    }, 
    {
      "default": "None", 
      "type": "", 
      "optional": "true", 
      "name": "init", 
      "description": "An estimator object that is used to compute the initial predictions. ``init`` has to provide ``fit`` and ``predict``. If None it uses ``loss.init_estimator``. "
    }, 
    {
      "type": "int", 
      "name": "verbose", 
      "description": "Enable verbose output. If 1 then it prints progress and performance once in a while (the more trees the lower the frequency). If greater than 1 then it prints progress and performance for every tree. "
    }, 
    {
      "type": "bool", 
      "name": "warm_start", 
      "description": "When set to ``True``, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just erase the previous solution. "
    }, 
    {
      "default": "None", 
      "type": "int", 
      "optional": "true", 
      "name": "random_state", 
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    }, 
    {
      "default": "\\'auto\\'", 
      "type": "bool", 
      "optional": "true", 
      "name": "presort", 
      "description": "Whether to presort the data to speed up the finding of best splits in fitting. Auto mode by default will use presorting on dense data and default to normal sorting on sparse data. Setting presort to true on sparse data will raise an error.  .. versionadded:: 0.17 optional parameter *presort*. "
    }
  ], 
  "input_type": [
    "DENSE", 
    "UNSIGNED_DATA"
  ], 
  "methods_available": [
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.apply", 
      "returns": {
        "shape": "n_samples, n_estimators", 
        "type": "array", 
        "name": "X_leaves", 
        "description": "For each datapoint x in X and for each tree in the ensemble, return the index of the leaf x ends up in each estimator. '"
      }, 
      "description": "'Apply trees in the ensemble to X, return leaf indices.\n\n.. versionadded:: 0.17\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The input samples. Internally, its dtype will be converted to ``dtype=np.float32``. If a sparse matrix is provided, it will be converted to a sparse ``csr_matrix``. "
        }
      ], 
      "name": "apply"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.decision_function", 
      "returns": {
        "shape": "n_samples, n_classes", 
        "type": "array", 
        "name": "score", 
        "description": "The decision function of the input samples. The order of the classes corresponds to that in the attribute `classes_`. Regression and binary classification produce an array of shape [n_samples]. '"
      }, 
      "description": "'DEPRECATED:  and will be removed in 0.19\n\nCompute the decision function of ``X``.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The input samples. "
        }
      ], 
      "name": "decision_function"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.fit", 
      "returns": {
        "type": "object", 
        "name": "self", 
        "description": "Returns self. '"
      }, 
      "description": "'Fit the gradient boosting model.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Training vectors, where n_samples is the number of samples and n_features is the number of features. "
        }, 
        {
          "shape": "n_samples", 
          "type": "array-like", 
          "name": "y", 
          "description": "Target values (integers in classification, real numbers in regression) For classification, labels must correspond to classes. "
        }, 
        {
          "shape": "n_samples", 
          "type": "array-like", 
          "name": "sample_weight", 
          "description": "Sample weights. If None, then samples are equally weighted. Splits that would create child nodes with net zero or negative weight are ignored while searching for a split in each node. In the case of classification, splits are also ignored if they would result in any single class carrying a negative weight in either child node. "
        }, 
        {
          "type": "callable", 
          "optional": "true", 
          "name": "monitor", 
          "description": "The monitor is called after each iteration with the current iteration, a reference to the estimator and the local variables of ``_fit_stages`` as keyword arguments ``callable(i, self, locals())``. If the callable returns ``True`` the fitting procedure is stopped. The monitor can be used for various things such as computing held-out estimates, early stopping, model introspect, and snapshoting. "
        }
      ], 
      "name": "fit"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.fit_transform", 
      "returns": {
        "shape": "n_samples, n_features_new", 
        "type": "numpy", 
        "name": "X_new", 
        "description": "Transformed array.  '"
      }, 
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "numpy", 
          "name": "X", 
          "description": "Training set. "
        }, 
        {
          "shape": "n_samples", 
          "type": "numpy", 
          "name": "y", 
          "description": "Target values. "
        }
      ], 
      "name": "fit_transform"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.get_params", 
      "returns": {
        "type": "mapping", 
        "name": "params", 
        "description": "Parameter names mapped to their values. '"
      }, 
      "description": "'Get parameters for this estimator.\n", 
      "parameters": [
        {
          "type": "boolean", 
          "optional": "true", 
          "name": "deep", 
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ], 
      "name": "get_params"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.predict", 
      "returns": {
        "shape": "n_samples", 
        "type": "array", 
        "name": "y", 
        "description": "The predicted values. '"
      }, 
      "description": "'Predict regression target for X.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The input samples. "
        }
      ], 
      "name": "predict"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.score", 
      "returns": {
        "type": "float", 
        "name": "score", 
        "description": "R^2 of self.predict(X) wrt. y. '"
      }, 
      "description": "'Returns the coefficient of determination R^2 of the prediction.\n\nThe coefficient R^2 is defined as (1 - u/v), where u is the regression\nsum of squares ((y_true - y_pred) ** 2).sum() and v is the residual\nsum of squares ((y_true - y_true.mean()) ** 2).sum().\nBest possible score is 1.0 and it can be negative (because the\nmodel can be arbitrarily worse). A constant model that always\npredicts the expected value of y, disregarding the input features,\nwould get a R^2 score of 0.0.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Test samples. "
        }, 
        {
          "shape": "n_samples", 
          "type": "array-like", 
          "name": "y", 
          "description": "True values for X. "
        }, 
        {
          "type": "array-like", 
          "shape": "n_samples", 
          "optional": "true", 
          "name": "sample_weight", 
          "description": "Sample weights. "
        }
      ], 
      "name": "score"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.set_params", 
      "returns": {
        "name": "self", 
        "description": "\""
      }, 
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n", 
      "parameters": [], 
      "name": "set_params"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.staged_decision_function", 
      "returns": {
        "shape": "n_samples, k", 
        "type": "generator", 
        "name": "score", 
        "description": "The decision function of the input samples. The order of the classes corresponds to that in the attribute `classes_`. Regression and binary classification are special cases with ``k == 1``, otherwise ``k==n_classes``. '"
      }, 
      "description": "'DEPRECATED:  and will be removed in 0.19\n\nCompute decision function of ``X`` for each iteration.\n\nThis method allows monitoring (i.e. determine error on testing set)\nafter each stage.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The input samples. "
        }
      ], 
      "name": "staged_decision_function"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.staged_predict", 
      "returns": {
        "shape": "n_samples", 
        "type": "generator", 
        "name": "y", 
        "description": "The predicted value of the input samples. '"
      }, 
      "description": "'Predict regression target at each stage for X.\n\nThis method allows monitoring (i.e. determine error on testing set)\nafter each stage.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The input samples. "
        }
      ], 
      "name": "staged_predict"
    }, 
    {
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor.transform", 
      "returns": {
        "shape": "n_samples, n_selected_features", 
        "type": "array", 
        "name": "X_r", 
        "description": "The input samples with only the selected features. '"
      }, 
      "description": "'DEPRECATED: Support to use estimators as feature selectors will be removed in version 0.19. Use SelectFromModel instead.\n\nReduce X to its most important features.\n\nUses ``coef_`` or ``feature_importances_`` to determine the most\nimportant features.  For models with a ``coef_`` for each class, the\nabsolute sum over the classes is used.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array", 
          "name": "X", 
          "description": "The input samples. "
        }, 
        {
          "default": "None", 
          "type": "string", 
          "optional": "true", 
          "name": "threshold", 
          "description": "The threshold value to use for feature selection. Features whose importance is greater or equal are kept while the others are discarded. If \"median\" (resp. \"mean\"), then the threshold value is the median (resp. the mean) of the feature importances. A scaling factor (e.g., \"1.25*mean\") may also be used. If None and if available, the object attribute ``threshold`` is used. Otherwise, \"mean\" is used by default. "
        }
      ], 
      "name": "transform"
    }
  ], 
  "common_name_unanalyzed": "Gradient Boosting Regressor", 
  "schema_version": 1.0, 
  "languages": [
    "python2.7"
  ], 
  "version": "0.18.1", 
  "build": [
    {
      "type": "pip", 
      "package": "scikit-learn"
    }
  ], 
  "handles_multiclass": false, 
  "description": "'Gradient Boosting for regression.\n\nGB builds an additive model in a forward stage-wise fashion;\nit allows for the optimization of arbitrary differentiable loss functions.\nIn each stage a regression tree is fit on the negative gradient of the\ngiven loss function.\n\nRead more in the :ref:`User Guide <gradient_boosting>`.\n", 
  "tags": [
    "ensemble", 
    "gradient_boosting"
  ], 
  "learning_type": [
    "supervised"
  ], 
  "task_type": [
    "modeling"
  ], 
  "output_type": [
    "PREDICTIONS"
  ], 
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/ensemble/gradient_boosting.py#L1635", 
  "category_unanalyzed": "ensemble.gradient_boosting", 
  "name": "sklearn.ensemble.gradient_boosting.GradientBoostingRegressor", 
  "handles_multilabel": false, 
  "is_deterministic": true, 
  "team": "jpl", 
  "attributes": [
    {
      "shape": "n_features", 
      "type": "array", 
      "name": "feature_importances_", 
      "description": "The feature importances (the higher, the more important the feature). "
    }, 
    {
      "shape": "n_estimators", 
      "type": "array", 
      "name": "oob_improvement_", 
      "description": "The improvement in loss (= deviance) on the out-of-bag samples relative to the previous iteration. ``oob_improvement_[0]`` is the improvement in loss of the first stage over the ``init`` estimator. "
    }, 
    {
      "shape": "n_estimators", 
      "type": "array", 
      "name": "train_score_", 
      "description": "The i-th score ``train_score_[i]`` is the deviance (= loss) of the model at iteration ``i`` on the in-bag sample. If ``subsample == 1`` this is the deviance on the training data. "
    }, 
    {
      "type": "", 
      "name": "loss_", 
      "description": "The concrete ``LossFunction`` object.  `init` : BaseEstimator The estimator that provides the initial predictions. Set via the ``init`` argument or ``loss.init_estimator``. "
    }, 
    {
      "shape": "n_estimators, 1", 
      "type": "ndarray", 
      "name": "estimators_", 
      "description": "The collection of fitted sub-estimators.  See also -------- DecisionTreeRegressor, RandomForestRegressor "
    }
  ]
}
