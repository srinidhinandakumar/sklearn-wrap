from typing import NamedTuple, Sequence, Any

from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

from d3m_metadata.sequence import ndarray
from d3m_metadata import hyperparams
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
Params = NamedTuple('Params', [
    ('estimators', ndarray),
    ('classes', ndarray),
    ('n_classes', int),
    ('estimator_weights', ndarray),
    ('estimator_errors', ndarray),
    ('feature_importances', ndarray)
])

Hyperparams = NamedTuple('Hyperparams', [
    ('base_estimator', object),
    ('learning_rate', float),
    ('n_estimators', int),
    ('algorithm', )
])


class AdaBoost(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Primitive wrapping for sklearn.ensemble.AdaBoostClassifier
    """

    __author__ = "JPL DARPA D3M TEAM"
    __metadata__ = {
        "common_name": "AdaBoost Classifier",
        "algorithm_type": ["Ensemble", "Decision Tree"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": []
        },
        "handles_regression": False,
        "handles_classification": True,
        "handles_multiclass": True,
        "handles_multilabel": True,
        "input_type": ["DENSE", "SPARSE"],
        "output_type": ["PREDICTIONS"]
    }

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 hyperparams: Hyperparams = Hyperparams,
                 random_seed: int = 0) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed)

        self._clf = AdaBoostClassifier(
            base_estimator=hyperparams.base_estimator,
            n_estimators=hyperparams.n_estimators,
            learning_rate=hyperparams.learning_rate,
            algorithm=hyperparams.algorithm,
            random_state=random_seed
        )

        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self.fitted:
            return

        if self.training_inputs is None or self.training_outputs is None:
            raise ValueError("Missing training data.")

        self._clf.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        return self._clf.predict(inputs)

    def get_params(self) -> Params:
        return Params(estimators=self._clf.estimators_,
                      classes=self._clf.classes_,
                      n_classes=self._clf.n_classes_,
                      estimator_weights=self._clf.estimator_weights_,
                      estimator_errors=self._clf.estimator_errors_,
                      feature_importances=self._clf.feature_importances_)

    def set_params(self, *, params: Params) -> None:
        self._clf.estimators_ = params.estimators
        self._clf.classes_ = params.classes
        self._clf.n_classes_ = params.n_classes
        self._clf.estimator_weights_ = params.estimator_weights
        self._clf.estimator_errors_ = params.estimator_errors
        # Property feature_importances cannot be set as it is computed.

    def get_hyperparams(self) -> hyperparams.Hyperparams:
        return hyperparams.Hyperparams(learning_rate=self._clf.learning_rate,
                                       n_estimators=self._clf.n_estimators,
                                       algorithm=self._clf.algorithm,
                                       max_depth=self._clf.base_estimator.max_depth)
