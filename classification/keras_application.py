from typing import NamedTuple, Sequence, Any, List, Dict, Union
from collections import OrderedDict

# import the necessary packages
from keras.applications import ResNet50
from keras.applications import InceptionV3
from keras.applications import Xception # TensorFlow ONLY
from keras.applications import VGG16
from keras.applications import VGG19
from keras.applications import imagenet_utils
from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img

from d3m_metadata.container.numpy import ndarray
from d3m_metadata import hyperparams, params, metadata as metadata_module
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

import numpy as np

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

MODELS = {
	"VGG16": VGG16,
	"VGG19": VGG19,
	"InceptionV3": InceptionV3,
	"Xception": Xception, # TensorFlow ONLY
	"ResNet50": ResNet50
}

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
class Params(params.Params):
    layers: List[object]
    inputs: List[object]
    outputs: List[object]

class Hyperparams(hyperparams.Hyperparams):
    model = hyperparams.Hyperparameter(
        default='VGG16',
        description='One of Keras Applications that are made available alongside pre-trained weights.',
        _structural_type=str
    )
    include_top = hyperparams.Hyperparameter(
        default=True,
        description='Whether to include the fully-connected layer at the top of the network.',
        _structural_type=bool
    )
    weights = hyperparams.Hyperparameter(
        default='imagenet',
        description='One of None (random initialization) or \'imagenet\' (pre-training on ImageNet).',
        _structural_type=object
    )
    input_tensor = hyperparams.Hyperparameter(
        default=None,
        description='Optional Keras tensor (i.e. output of layers.Input()) to use as image input for the model.',
        _structural_type=object
    )
    input_shape = hyperparams.Hyperparameter(
        default=None,
        description='Optional shape tuple, only to be specified if include_top is False.',
        _structural_type=object
    )
    pooling = hyperparams.Hyperparameter(
        default=None,
        description='Optional pooling mode for feature extraction when include_top is False.',
        _structural_type=object
    )
    classes = hyperparams.Hyperparameter(
        default=1000,
        description='Optional number of classes to classify images into, only to be specified if include_top is True, and if no weights argument is specified.',
        _structural_type=int
    )

class KerasApplication(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Primitive wrapping for Keras Applications
    """

    __author__ = "JPL DARPA D3M TEAM"
    metadata = metadata_module.PrimitiveMetadata(
        {
            "id": "test",
            "common_name": "Keras Application",
            "algorithm_types": [
                "CLASSIFIER_CHAINS"
            ],
            "compute_resources": {},
            "input_type": [
                "DENSE",
                "SPARSE",
                "UNSIGNED_DATA"
            ],
            "output_type": [
                "PREDICTIONS"
            ],
            "installation": [
                {
                    "type": "PIP",
                    "package_uri": "https://gitlab.datadrivendiscovery.org/jpl/d3m_sklearn_wrap.git"
                }],
            "name": "KerasApplication",
            "primitive_family": "CLASSIFICATION",
            "python_path": "d3m.primitives.sklearn_wrap.keras_application",
            "source": {"name": "JPL D3M"},
            "version": "0.1"
        }
    )

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, str] = None) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)
        Network = MODELS[hyperparams['model']]
        self._clf = Network(
            include_top=hyperparams['include_top'],
            weights=hyperparams['weights'],
            input_tensor=hyperparams['input_tensor'],
            input_shape=hyperparams['pooling'],
            classes=hyperparams['classes']
        )
        #
        # self.training_inputs = None  # type: ndarray
        # self.training_outputs = None  # type: ndarray
        # self.fitted = False

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = inputs
        self._training_outputs = outputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self._fitted:
            return

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        self._clf.fit(self._training_inputs, self._training_outputs)
        self._fitted = True

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        preprocess = imagenet_utils.preprocess_input

        hyperparams = self.get_hyperparams()

        # if we are using the InceptionV3 or Xception networks, then we
        # need to set the input shape to (299x299) [rather than (224x224)]
        # and use a different image processing function
        if hyperparams['model'] in ("InceptionV3", "Xception"):
            preprocess = preprocess_input

        # our input image is now represented as a NumPy array of shape
        # (inputShape[0], inputShape[1], 3) however we need to expand the
        # dimension by making the shape (1, inputShape[0], inputShape[1], 3)
        # so we can pass it through thenetwork
        image = np.expand_dims(inputs, axis=0)

        # pre-process the image using the appropriate function based on the
        # model that has been loaded (i.e., mean subtraction, scaling, etc.)
        image = preprocess(image)

        # classify the image
        return imagenet_utils.decode_predictions(self._clf.predict(image))

    def get_params(self) -> Params:
        return Params(
            layers = self._clf.layers,
            inputs = self._clf.inputs,
            outputs = self._clf.outputs
            )

    def set_params(self, *, params: Params) -> None:
        self._clf.layers = params.layers
        self._clf.inputs = params.inputs
        self._clf.outputs = params.outputs

    def get_hyperparams(self) -> hyperparams.Hyperparams:
        return self.hyperparams