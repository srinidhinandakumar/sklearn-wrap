from typing import NamedTuple, Sequence, Any

from sklearn.naive_bayes import BernoulliNB as BNB

from d3m_metadata.sequence import ndarray
from d3m_metadata import hyperparams
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = Sequence[Any]

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
Params = NamedTuple('Params', [
    ('class_log_prior', ndarray),
    ('feature_log_prob', ndarray),
    ('class_count', ndarray),
    ('feature_count', ndarray)
])

Hyperparams = NamedTuple('Hyperparams', [
    ('alpha', hyperparams.Hyperparams),
    ('fit_prior', hyperparams.Hyperparams)
])


class BernoulliNB(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Primitive wrapping for sklearn.pipeline.components.classification.beranoulli_nb.
    """

    __author__ = "JPL DARPA D3M TEAM"
    __metadata__ = {
        "common_name": "Bernoulli Naive Bayes Classifier",
        "algorithm_type": ["Naive Bayes"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": []
        },
        "handles_regression": False,
        "handles_classification": True,
        "handles_multiclass": True,
        "handles_multilabel": True,
        "input_type": ["DENSE", "SPARSE", "UNSIGNED_DATA"],
        "output_type": ["PREDICTIONS"]
    }

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 alpha: float = 1.0,
                 binarize: float = 0.0,
                 fit_prior: bool = True,
                 class_prior: ndarray = None) -> None:

        super().__init__(hyperparams=Hyperparams)

        self.bernoulli_nb = BNB(
            alpha=alpha,
            binarize=binarize,
            fit_prior=fit_prior,
            class_prior=class_prior
        )

        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: Sequence[Any]
        self.fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        return self.bernoulli_nb.predict(inputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self.fitted:
            return

        if self.training_inputs is None or self.training_outputs is None:
            raise ValueError("Missing training data.")

        self.bernoulli_nb.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self) -> Params:
        return Params(class_log_prior=self.bernoulli_nb.class_log_prior_,
                      feature_log_prob=self.bernoulli_nb.feature_log_prob_,
                      class_count=self.bernoulli_nb.class_count_,
                      feature_count=self.bernoulli_nb.feature_count_)

    def set_params(self, *, params: Params):
        self.bernoulli_nb.class_log_prior_ = params.class_log_prior
        self.bernoulli_nb.feature_log_prob_ = params.feature_log_prob
        self.bernoulli_nb.class_count_ = params.class_count
        self.bernoulli_nb.feature_count_ = params.feature_count

    def get_hyperparams(self) -> Hyperparams:
        return Hyperparams(alpha=self.bernoulli_nb.alpha,
                           fit_prior=self.bernoulli_nb.fit_prior)
