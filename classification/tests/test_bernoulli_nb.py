import unittest
import numpy as np

from classification import bernoulli_nb

result = [3]

class TestBernoulliNB(unittest.TestCase):

    def test_produce(self):
        X = np.random.randint(2, size=(6, 100))
        Y = np.array([1, 2, 3, 4, 4, 5])
        clf = bernoulli_nb.BernoulliNB()
        clf.set_training_data(inputs=X, outputs=Y)
        clf.fit()

        assert np.array_equal(clf.produce(inputs=X[2:3]), result)


if __name__ == '__main__':
    unittest.main()
