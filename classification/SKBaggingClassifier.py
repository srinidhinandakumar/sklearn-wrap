from typing import Any, Callable, List, Dict, Union, Optional, Sequence, Tuple
from numpy import ndarray
from collections import OrderedDict
from scipy import sparse
import os
import sklearn

# Custom import commands if any
from sklearn.ensemble import BaggingClassifier
import numpy
from typing import Optional
from d3m.container.numpy import ndarray as d3m_ndarray
from d3m.container import DataFrame as d3m_DataFrame
from d3m.metadata import hyperparams, params, base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

Inputs = Union[d3m_DataFrame, d3m_ndarray]
Outputs = d3m_ndarray


class Params(params.Params):
    base_estimator_: Optional[object]
    estimators_: Optional[List[sklearn.tree.DecisionTreeClassifier]]
    estimators_samples_: Optional[List[ndarray]]
    estimators_features_: Optional[List[ndarray]]
    classes_: Optional[ndarray]
    n_classes_: Optional[int]
    oob_score_: Optional[float]
    oob_decision_function_: Optional[List[ndarray]]  # array of shape = [n_samples, n_classes] - how to fix ndarray size
    n_features_: Optional[int]


class Hyperparams(hyperparams.Hyperparams):
    base_estimator = hyperparams.Hyperparameter[Union[object, None]](
        default=None,
        description="The base estimator to fit on random subsets of the dataset. If None, then the base estimator is a decision tree.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    n_estimator = hyperparams.Hyperparameter[int](
        default=10,
        description="The number of base estimators in the ensemble.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    max_samples = hyperparams.Hyperparameter[Union[int, float]](
        default=1.0,
        description="Optional. The number of samples to draw from X to train each base estimator. If int, then draw max_samples samples.If float, then draw max_samples * X.shape[0] samples.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    max_features = hyperparams.Hyperparameter[Union[int, float]](
        default=1.0,
        description="Optional. The number of features to draw from X to train each base estimator.If int, then draw max_features features.If float, then draw max_features * X.shape[1] features.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    bootstrap = hyperparams.Hyperparameter[bool](
        default=True,
        description="Whether samples are drawn with replacement.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    bootstrap_features = hyperparams.Hyperparameter[bool](
        default=False,
        description="Whether features are drawn with replacement.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    oob_score = hyperparams.Hyperparameter[bool](
        default=False, # not sure
        description="Whether to use out-of-bag samples to estimate the generalization error.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    warm_start = hyperparams.Hyperparameter[bool](
        default=False,
        description="When set to True, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just fit a whole new ensemble.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    n_jobs = hyperparams.Hyperparameter[int](
        default=1,
        description="The number of jobs to run in parallel for both fit and predict. If -1, then the number of jobs is set to the number of cores.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    random_state = hyperparams.Hyperparameter[Union[int, object, None]](
        default=None,
        description="If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    verbose = hyperparams.Hyperparameter[int](
        default=0,
        description="Controls the verbosity of the building process.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )


class SKBaggingClassifier(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
        Primitive wrapping for sklearn BaggingClassifier
    """

    __author__ = "JPL MARVIN"
    metadata = metadata_module.PrimitiveMetadata({
         "algorithm_types": ['ENSEMBLE_LEARNING'],
         "name": "sklearn.ensemble.BaggingClassifier",
         "primitive_family": "CLASSIFICATION",
         "python_path": "d3m.primitives.sklearn_wrap.SKBaggingClassifier",
         "source": {'name': 'JPL'},
         "version": "0.1.0",
         "id": "e20d003d-6a9f-35b0-b4b5-20e42b30282a",
         'installation': [{'type': metadata_module.PrimitiveInstallationType.PIP,
                           'package_uri': 'git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@{git_commit}#egg=sklearn_wrap'.format(
                               git_commit=utils.current_git_commit(os.path.dirname(__file__)),
                            ),
                           }]
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed)

        self._clf = BaggingClassifier(base_estimator=self.hyperparams['base_estimator'],
                                      n_estimators=self.hyperparams['n_estimator'],
                                      max_samples=self.hyperparams['max_samples'],
                                      max_features=self.hyperparams['max_features'],
                                      bootstrap=self.hyperparams['bootstrap'],
                                      bootstrap_features=self.hyperparams['bootstrap_features'],
                                      oob_score=self.hyperparams['oob_score'],
                                      warm_start=self.hyperparams['warm_start'],
                                      n_jobs=self.hyperparams['n_jobs'],
                                      random_state=self.hyperparams['random_state'],
                                      verbose=self.hyperparams['verbose']
                                      )
        self._training_inputs = None
        self._training_outputs = None
        self._fitted = False

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = inputs
        self._training_outputs = outputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        self._clf.fit(self._training_inputs, self._training_outputs)
        self._fitted = True

        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        output = self._clf.predict(inputs)
        if sparse.issparse(output):
            output = output.toarray()
        return CallResult(d3m_ndarray(output))

    def get_params(self) -> Params:

        if not self._fitted:
            raise ValueError("Fit not performed.")

        return Params(base_estimator_=getattr(self._clf, 'base_estimator_', None),
                      estimators_=getattr(self._clf, 'estimators_', None),
                      estimators_samples_=getattr(self._clf, 'estimators_samples_', None),
                      estimators_features_=getattr(self._clf, 'estimators_features_', None),
                      classes_=getattr(self._clf, 'classes_', None),
                      n_classes_=getattr(self._clf, 'n_classes_', None),
                      oob_score_=getattr(self._clf, 'oob_score_', None),
                      oob_decision_function_=getattr(self._clf, 'oob_decision_function_', None),
                      n_features_=getattr(self._clf, 'n_features_', None)
                      )

    def set_params(self, *, params: Params) -> None:
        self._clf.base_estimator_ = params['base_estimator_']
        self._clf.estimators_ = params['estimators_']
        self._clf.estimators_features_ = params['estimators_features_']
        self._clf.classes_ = params['classes_']
        self._clf.n_classes_ = params['n_classes_']
        self._clf.oob_score_ = params['oob_score_']
        self._clf.oob_decision_function_ = params['oob_decision_function_']
        self._clf.n_features_ = params['n_features_']
        self._fitted = True


SKBaggingClassifier.__doc__ = BaggingClassifier.__doc__
